﻿Imports System.Data
Public Class LegalSearch
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadKeyword()
            LoadBusinessType()
            LoadMinistry()
            LoadYear()
            LoadLegal()

        End If
    End Sub
    Private Sub LoadLegal()
        Select Case Request("s")
            Case "1"
                dtLegal = ctlL.Legal_GetByPhaseUID(StrNull2Zero(ddlPhase.SelectedValue))
            Case "2"
                dtLegal = ctlL.Legal_GetByMinistryUID(StrNull2Zero(ddlPhase.SelectedValue), StrNull2Zero(ddlMinistry.SelectedValue))
            Case "3"
                dtLegal = ctlL.Legal_GetByYear(StrNull2Zero(ddlPhase.SelectedValue), StrNull2Zero(ddlYear.SelectedValue))
            Case "4"
                dtLegal = ctlL.Legal_GetByBusinessTypeUID(StrNull2Zero(ddlPhase.SelectedValue), StrNull2Zero(ddlBusinessType.SelectedValue))
            Case "5"
                Dim sKeyword As String = ""
                For i = 0 To chkKeyword.Items.Count - 1
                    If chkKeyword.Items(i).Selected Then
                        sKeyword = sKeyword + chkKeyword.Items(i).Value + ","
                    End If
                Next
                If sKeyword.Length > 1 Then
                    sKeyword = Left(sKeyword, sKeyword.Length - 1)
                End If

                dtLegal = ctlL.Legal_GetByKeyword(ddlPhase.SelectedValue, sKeyword)
        End Select

    End Sub
    Private Sub LoadKeyword()
        chkKeyword.DataSource = ctlM.Keyword_Get()
        chkKeyword.DataTextField = "Name"
        chkKeyword.DataValueField = "UID"
        chkKeyword.DataBind()
    End Sub
    Private Sub LoadBusinessType()
        ddlBusinessType.DataSource = ctlM.BusinessType_Get()
        ddlBusinessType.DataTextField = "Name"
        ddlBusinessType.DataValueField = "UID"
        ddlBusinessType.DataBind()
    End Sub
    Private Sub LoadMinistry()
        ddlMinistry.DataSource = ctlM.Ministry_Get()
        ddlMinistry.DataTextField = "Name"
        ddlMinistry.DataValueField = "UID"
        ddlMinistry.DataBind()
    End Sub
    Private Sub LoadYear()
        ddlYear.DataSource = ctlL.LegalYear_Get()
        ddlYear.DataTextField = "LegalYear"
        ddlYear.DataValueField = "LegalYear"
        ddlYear.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadLegal()
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        chkKeyword.ClearSelection()
    End Sub
End Class