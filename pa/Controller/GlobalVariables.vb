﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

Public Class GlobalVariables
        Public Shared BYear As Integer = 2021
        Public Shared username As String
        Public Shared usercode As String
        Public Shared LoginUser As String
        Public Shared IsHR As Boolean
        Public Shared IsAdmin As Boolean
        Public Shared IsSuperAdmin As Boolean
        Public Shared IsReporter As Boolean
        Public Shared IsDirector As Boolean
        Public Shared AssessorLevelUID As Integer
        Public Shared AssesseeLevelUID As Integer
        Public Shared DivisionUID As Integer
        Public Shared DepartmentUID As Integer
        Public Shared AssessmentClass As String
        Public Shared accessright As String
        Public Shared UseDatabase As String
        Public Shared UserEmployeeID As String
        Public Shared UserEmployeeName As String
        Public Shared UserEmployeeLevelID As String
        Public Shared UserEmployeeLevelName As String
        Public Shared DateTimeStamp As String
        Public Shared LocationClinic As String
        Public Shared ReportPath As String = ""
        Public Shared Reportskey As String = ""
        Public Shared ReportName As String = ""
        Public Shared ReportTitle As String = ""
        Public Shared FagRPT As String = ""
        Public Shared RPTTYPE As String = ""
        Public Shared RPTKEY As String = ""
        Public Shared CFG_STARTDATE As String = "STARTDATE"
        Public Shared CFG_ENDDATE As String = "ENDDATE"
        Public Shared CFG_PAYEAR As String = "PAYEAR"
        Public Shared CFG_RNDNO As String = "RNDNO"
        Public Shared FlagRefresh As Boolean = False
    End Class


