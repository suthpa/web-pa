﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Data



Public Class EmployeeController
        Inherits BaseClass

        Public ds As DataSet = New DataSet()

        Public Function Employee_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetJob(ByVal EmployeeID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetJob"), EmployeeID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetBannerInfo(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetBannerInfo"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetLevel() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetLevel"))
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetDepartment() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDepartment"))
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetDepartmentByDivision(ByVal DivUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDepartmentByDivision"), DivUID)
            Return ds.Tables(0)
        End Function

        Public Function GetDirection() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Direction_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetDirection(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDirection"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetDivision() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDivision"))
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetDivisionByDirection(ByVal DirectionUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetDivisionByDirection"), DirectionUID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetPosition() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetPosition"))
            Return ds.Tables(0)
        End Function

        Public Function Director_GetDirection(ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Director_GetDirection"), EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetEmployeeDirection(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDirection"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetEmployeeDivision(ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDivision"), EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetEmployeeDepartment(ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetEmployeeDepartment"), EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_Login(ByVal BYear As Integer, ByVal pEmployeename As String, ByVal pPassword As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_Login", BYear, pEmployeename, pPassword)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetPassword(ByVal pEmployeeID As String) As String
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_GetPassword", pEmployeeID)

            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0).ToString()
            Else
                Return "N/A"
            End If
        End Function

        Public Function Employee_ChangePassword(ByVal pEmployeeID As String, ByVal pPassword As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, "Employee_ChangePassword", pEmployeeID, pPassword)
        End Function

        Public Function Employee_NotAssessment(ByVal pYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_Employee_NotAssessment", pYear)
            Return ds.Tables(0)
        End Function

        Public Function EmployeeRelationAssessment(ByVal pYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_EmployeeRelationAssessment", pYear)
            Return ds.Tables(0)
        End Function

        Public Function Employee_IsSuperEmployee(ByVal Employeename As String) As Boolean
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_IsSuperEmployee"), Employeename)

            If ds.Tables(0).Rows.Count > 0 Then

                If DBNull2Zero(ds.Tables(0).Rows(0)(0)) = 1 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Public Function Employee_GetByCode(ByVal pCode As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetByCode"), pCode)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetByID(ByVal pID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetByID"), pID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessorStatus(ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorStatus"), EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessorTOPDetail(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorTOPDetail"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessorDetail(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetail"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessorDetail4Relation(ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetail4Relation"), EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessorDetailNew(ByVal BYear As Integer, ByVal EmpID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessorDetailNew"), BYear, EmpID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees(ByVal BYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees"), BYear)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees(ByVal BYear As Integer, ByVal LevelUID As String, ByVal DivisionUID As String, ByVal DepartmentUID As String, ByVal EmployeeID As String, ByVal AsmClass As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2021"), BYear, LevelUID, DivisionUID, DepartmentUID, EmployeeID, AsmClass)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseeManager(ByVal BYear As Integer, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseeManager"), BYear, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetNewAssesseeHeader(ByVal BYear As Integer, ByVal DivisionUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseeHeader"), BYear, DivisionUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseeHeader(ByVal BYear As Integer, ByVal DivisionUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseeHeader"), BYear, DivisionUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseesByDepartment(ByVal BYear As Integer, ByVal DepartmentUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByDepartment"), BYear, DepartmentUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetNewAssesseesByDepartment(ByVal BYear As Integer, ByVal DepartmentUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseesByDepartment"), BYear, DepartmentUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees2LevelByTOP(ByVal BYear As Integer, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByTOP"), BYear, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees2LevelByDirector(ByVal BYear As Integer, ByVal DirectionUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByDirector"), BYear, DirectionUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees2LevelByManager(ByVal BYear As Integer, ByVal DivisionUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByManager"), BYear, DivisionUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssessees2LevelByHeader(ByVal BYear As Integer, ByVal DepartmentUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssessees2LevelByHeader"), BYear, DepartmentUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseesByTOP2021(ByVal BYear As Integer, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByTOP2021"), BYear, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseesByDirector2021(ByVal BYear As Integer, ByVal DirectionUID As String, ByVal AssessorID As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByDirector2021"), BYear, DirectionUID, AssessorID)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseesByManager2021(ByVal BYear As Integer, ByVal DivisionUID As String, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal AsmClass As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByManager2021"), BYear, DivisionUID, AssessorID, LevelID, AsmClass)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetAssesseesByHeader2021(ByVal BYear As Integer, ByVal DivisionUID As String, ByVal DepartmentUID As String, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal AsmClass As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAssesseesByHeader2021"), BYear, DivisionUID, DepartmentUID, AssessorID, LevelID, AsmClass)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationByTOP(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByTOP"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function RPT_HODRelation(ByVal BYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_HODRelation"), BYear)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationByDirector(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByDirector"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationByManager(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByManager"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationByHeader(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByHeader"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationBySubHeader(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationBySubHeader"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetRelationByAssessor(ByVal BYear As Integer, ByVal AssessorID As String, ByVal LevelID As Integer, ByVal RNDNO As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetRelationByAssessor"), BYear, AssessorID, LevelID, RNDNO)
            Return ds.Tables(0)
        End Function

        Public Function Employee_GetNewAssesseesByHR(ByVal BYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetNewAssesseesByHR"), BYear)
            Return ds.Tables(0)
        End Function

        Public Function Employee_LastLog_Update(ByVal pEmployeename As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_LastLog_Update"), pEmployeename)
        End Function

        Public Function Employee_Delete(ByVal pID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Delete"), pID)
        End Function

        Public Function Employee_Add(ByVal Employeename As String, ByVal Password As String, ByVal FName As String, ByVal LName As String, ByVal Email As String, ByVal IsSuperEmployee As Integer, ByVal Status As Integer, ByVal EmployeeProfileID As Integer, ByVal ProfileID As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Add"), Employeename, Password, FName, LName, Email, IsSuperEmployee, Status, EmployeeProfileID, ProfileID)
        End Function

        Public Function Employee_Update(ByVal Employeename As String, ByVal Password As String, ByVal FName As String, ByVal LName As String, ByVal Email As String, ByVal IsSuperEmployee As Integer, ByVal Status As Integer, ByVal EmployeeProfileID As Integer, ByVal ProfileID As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Update"), Employeename, Password, FName, LName, Email, IsSuperEmployee, Status, EmployeeProfileID, ProfileID)
        End Function

        Public Function EmployeeRole_Add(ByVal Employeename As String, ByVal RoleID As Integer, ByVal UpdBy As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeRole_Add"), Employeename, RoleID, UpdBy)
        End Function

        Public Function GEN_Employee_PrintAssessment(ByVal Year As Integer, ByVal LevelID As Integer, ByVal DirID As Integer, ByVal DivID As Integer, ByVal DeptID As Integer, ByVal StartCode As String, ByVal EndCode As String, ByVal Username As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_Employee_PrintAssessment"), Year, LevelID, DirID, DivID, DeptID, StartCode, EndCode, Username)
        End Function

        Public Function GEN_Employee_FinalScoreByAssessor(ByVal Year As Integer, ByVal DeptID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_Employee_FinalScoreByAssessor"), Year, DeptID)
        End Function

        Public Function GEN_EmployeeAssessmentFinalScore(ByVal Year As Integer, ByVal DeptID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScore"), Year, DeptID)
        End Function

        Public Function GEN_EmployeeAssessmentFinalScoreDirection(ByVal Year As Integer, ByVal DirectionID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScoreDirection"), Year, DirectionID)
        End Function

        Public Function GEN_EmployeeAssessmentFinalScoreDivision(ByVal Year As Integer, ByVal DivisionUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("GEN_EmployeeAssessmentFinalScoreDivision"), Year, DivisionUID)
        End Function

        Public Function Employee_UpdateProfileID(ByVal Employeename As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_UpdateProfileID"), Employeename)
        End Function

        Public Function Employee_GenLogfile(ByVal Employeename As String, ByVal Act_Type As String, ByVal DB_Effective As String, ByVal Descrp As String, ByVal Remark As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_genLogfile"), Employeename, Act_Type, DB_Effective, Descrp, Remark)
        End Function

        Public Function Employee_UpdateMail(ByVal Employeename As String, ByVal email As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_UpdateEmail"), Employeename, email)
        End Function

        Public Function Employee_ReasonFinalSave(ByVal PatientUID As Long, ByVal PatientVisitUID As Long, ByVal Employeename As String, ByVal ReasonTxT As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_ReasonFinalSave"), PatientUID, PatientVisitUID, Employeename, ReasonTxT)
        End Function

        Public Function User_GetAllowPermission(ByVal Username As String) As Boolean
            ds = SqlHelper.ExecuteDataset(ConnectionString, "User_GetAllowPermission", Username)

            If Convert.ToInt32(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function Employee_Duplicate(ByVal EmployeeID As String) As Boolean
            ds = SqlHelper.ExecuteDataset(ConnectionString, "Employee_Duplicate", EmployeeID)

            If Convert.ToInt32(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function Employee_Save(ByVal UID As Integer, ByVal EmployeeID As String, ByVal PositionUID As Integer, ByVal DepartmentUID As Integer, ByVal DivisionUID As Integer, ByVal DirectionUID As Integer, ByVal Title As String, ByVal Name As String, ByVal Password As String, ByVal isAdmin As String, ByVal isReporter As String, ByVal isSuperAdmin As String, ByVal isMedical As String, ByVal isHR As String, ByVal NationalID As String, ByVal HireDate As String, ByVal StatusFlag As String, ByVal StatusAssessment As String, ByVal LevelID As Integer, ByVal isProbation As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Save"), UID, EmployeeID, DepartmentUID, DivisionUID, DirectionUID, Title, Name, PositionUID, Password, isAdmin, isReporter, isSuperAdmin, isMedical, isHR, NationalID, HireDate, StatusFlag, StatusAssessment, LevelID, isProbation)
        End Function

        Public Function Employee_SaveByImport(ByVal EmployeeID As String, ByVal Title As String, ByVal Fname As String, ByVal Lname As String, ByVal PositionName As String, ByVal DepartmentName As String, ByVal DivisionName As String, ByVal HireDate As String, ByVal isMedical As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_SaveByImport"), EmployeeID, Title, Fname, Lname, PositionName, DepartmentName, DivisionName, HireDate, isMedical)
        End Function

        Public Function Employee_Disable(ByVal UID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Employee_Disable"), UID)
        End Function

        Public Function EmployeeStatus_Save(ByVal UID As Integer, ByVal BYear As Integer, ByVal EmployeeID As String, ByVal PositionUID As Integer, ByVal DepartmentUID As Integer, ByVal DivisionUID As Integer, ByVal DirectionUID As Integer, ByVal StatusFlag As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatus_Save"), UID, BYear, EmployeeID, PositionUID, DepartmentUID, DivisionUID, DirectionUID, StatusFlag)
        End Function

        Public Function EmployeeStatusMain_Save(ByVal BYear As Integer, ByVal EmployeeID As String, ByVal PositionUID As Integer, ByVal DepartmentUID As Integer, ByVal DivisionUID As Integer, ByVal DirectionUID As Integer, ByVal StatusFlag As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatusMain_Save"), BYear, EmployeeID, PositionUID, DepartmentUID, DivisionUID, DirectionUID, StatusFlag)
        End Function

        Public Function EmployeeStatus_Delete(ByVal UID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeStatus_Delete"), UID)
        End Function
    End Class

