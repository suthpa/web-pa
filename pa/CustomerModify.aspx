﻿<%@ Page Title="ข้อมูล Customer User" MetaDescription="จัดการข้อมูลลูกค้า (Customer User)" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CustomerModify.aspx.vb" Inherits="CustomerModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user-female icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content"> 
    <asp:Label ID="lblAlert1" runat="server" cssclass="alert alert-error alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  

<div class="row">
   <section class="col-lg-6 connectedSortable">
  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-warm-flame">
            </i>ข้อมูลทั่วไป
            <div class="btn-actions-pane-right">
              <asp:HiddenField ID="hdCustomerUID" runat="server" /> 
            </div>
        </div>
        <div class="card-body">
      <div class="row">          
         <div class="col-md-12">
          <div class="form-group">
            <label>Company</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>
        </div>
      </div>
      <div class="row">
          <div class="col-md-6">
          <div class="form-group">
            <label>ชื่อ</label>
            <asp:TextBox ID="txtFirstName" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>นามสกุล</label>
            <asp:TextBox ID="txtLastName" runat="server" cssclass="form-control" placeholder="นามสกุล"></asp:TextBox>          
          </div>
        </div> 
      </div>
      <div class="row">          
           <div class="col-md-6">
          <div class="form-group">
            <label>Telephone</label>
            <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>E-mail</label>
            <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
          </div>
        </div>
      </div>    
    </div>
  </div>
  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-screen icon-gradient bg-warm-flame">
            </i>รหัสผู้ใช้งาน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Username</label>
            <asp:TextBox ID="txtUsername" runat="server" cssclass="form-control text-center" placeholder="ชื่อ Login"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Password</label>
            <asp:TextBox ID="txtPassword" runat="server" cssclass="form-control text-center" placeholder="รหัสผ่าน"></asp:TextBox>
          </div>
      </div>
      </div>
</div> 
        <div class="card-footer clearfix">           
                
            </div>
          </div>  
   </section>
    <section class="col-lg-6 connectedSortable">
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-checkmark-circle icon-gradient bg-warm-flame"></i>Approver Assignment
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body"> 
            <div class="row">
                
        <table class="table">
            <tr>
                <td width="80px">Approver</td>
                <td> <asp:DropDownList ID="ddlApprover" runat="server" cssclass="form-control select2"  placeholder="--- เลือก Approver ---" Width="100%">
            </asp:DropDownList> </td>
                <td Width="80px"><asp:Button ID="cmdAddApprover" runat="server" Text="Add" Width="80" CssClass="btn btn-primary" /></td>
            </tr>
            <tr>
                <td colspan="3">               
                <asp:GridView ID="grdApprover" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-striped" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />                            </asp:BoundField>
                        <asp:BoundField DataField="ApproverName" HeaderText="Name" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
               </td>
            </tr>
        </table>        

            </div>
</div>        
          </div>         
        </section>                   
                </div>

     <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" Checked="True" />
          </div>
        </div>
        <div class="col-md-6">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdDel" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
        </div>
      </div>
 <asp:Label ID="lblAlert2" runat="server" cssclass="alert alert-error alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  


  
            <div class="row">
                </div>
     
          </section>
</asp:Content>