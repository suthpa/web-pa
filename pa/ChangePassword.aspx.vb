﻿Public Class ChangePassword
    Inherits System.Web.UI.Page

    Dim acc As New EmployeeController
    Dim enc As New CryptographyEngine

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnResult.Visible = False
            pnForm.Visible = True
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If Request.Cookies("iPA")("Password") <> txtOld.Text Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','รหัสผ่านเดิมท่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If
        acc.Employee_ChangePassword(Request.Cookies("iPA")("username"), enc.EncryptString(txtNew.Text, True))
        ' DisplayMessage(Me.Page, "ระบบเปลี่ยนรหัสผ่านให้ท่านใหม่เรียบร้อยแล้ว")

        pnForm.Visible = False
        pnResult.Visible = True
    End Sub
End Class