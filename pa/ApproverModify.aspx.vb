﻿Public Class ApproverModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CustomerController
    Dim ctlA As New ApproverController
    Dim ctlC As New CompanyController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblAlert1.Visible = False
            lblAlert2.Visible = False

            LoadCustomer()

            If Not Request("uid") Is Nothing Then
                LoadApproverData(Request("uid"))
            End If

            If Request.Cookies("iLaw")("ROLE_ADM") = True Then
                cmdDel.Visible = True
            Else
                cmdDel.Visible = False
            End If
        End If
    End Sub

    Private Sub LoadCustomer()
        ddlCustomer.DataSource = ctlE.Customer_GetActive()
        ddlCustomer.DataTextField = "Name"
        ddlCustomer.DataValueField = "UserID"
        ddlCustomer.DataBind()
    End Sub

    Private Sub LoadApproverData(ApproverUID As Integer)
        dt = ctlA.Approver_GetByApproverUID(ApproverUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdApproverUID.Value = String.Concat(.Item("ApproverUID"))
                txtFirstName.Text = String.Concat(.Item("FName"))
                txtLastName.Text = String.Concat(.Item("Lname"))
                txtTel.Text = String.Concat(.Item("Telephone"))
                txtEmail.Text = String.Concat(.Item("Email"))

                txtUsername.Text = String.Concat(.Item("Username"))
                txtPassword.Text = enc.DecryptString(String.Concat(.Item("Passwords")), True)

                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                LoadCustomerToGrid()
            End With

        Else
        End If
    End Sub

    Dim ctlU As New EmployeeController

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกชื่อ');", True)
            Exit Sub
        End If
        If txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกนามสกุล');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก E-mail');", True)
            Exit Sub
        End If

        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก Username');", True)
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Password');", True)
            Exit Sub
        End If

        ctlA.Approver_Save(StrNull2Zero(hdApproverUID.Value), txtUsername.Text, enc.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, txtTel.Text, txtEmail.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iLaw")("userid"))


        '--------------Update Customer-----------------
        Dim iApproverUID As Integer

        If StrNull2Zero(hdApproverUID.Value) = 0 Then
            If txtUsername.Text <> "" Then
                iApproverUID = ctlA.Approver_GetApproverUID(txtUsername.Text, txtFirstName.Text.Trim())
                ctlA.ApproverCustomer_UpdateApproverUID(txtUsername.Text, iApproverUID)
            End If
        End If
        '-----------------End Add -----------

        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Approver", "บันทึก/แก้ไข Approver User :{uid=" & hdApproverUID.Value & "}{username=" & txtUsername.Text & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("Approver.aspx?m=u&s=apv")

    End Sub

    Protected Sub cmdAddCustomer_Click(sender As Object, e As EventArgs) Handles cmdAddCustomer.Click
        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากำหนด Username ก่อน');", True)
            Exit Sub
        End If
        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลผู้ใช้ให้ครบถ้วนก่อน');", True)
            Exit Sub
        End If
        '--------------Add Approver-----------------
        Dim iApproverUID As Integer = StrNull2Zero(hdApproverUID.Value)
        ctlA.ApproverCustomer_Save(ddlCustomer.SelectedValue, iApproverUID, txtUsername.Text, Request.Cookies("iLaw")("UserID"))
        '-----------------End Add -----------
        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Approver", "บันทึก/แก้ไข Approver Asignment :{uid=" & hdApproverUID.Value & "}{username=" & txtUsername.Text & "}{approver=" & ddlCustomer.SelectedValue & "}", "")

        LoadCustomerToGrid()
    End Sub

    Private Sub LoadCustomerToGrid()
        Dim dtA As New DataTable
        dtA = ctlE.Customer_GetByApproverUID(StrNull2Zero(hdApproverUID.Value))
        If dtA.Rows.Count > 0 Then
            With grdCustomer
                .Visible = True
                .DataSource = dtA
                .DataBind()
            End With
        Else
            grdCustomer.DataSource = Nothing
            grdCustomer.Visible = False
        End If
        dtA = Nothing
    End Sub
    Protected Sub grdCustomer_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCustomer.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    If ctlA.ApproverCustomer_Delete(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), "DEL", "Approver", "ลบ Approver Customer :" & e.CommandArgument, "")
                        LoadCustomerToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub
End Class