﻿Imports System.Data
Public Class Approver
    Inherits System.Web.UI.Page
    Dim ctlA As New ApproverController
    Public dtApv As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        dtApv = ctlA.Approver_GetAll
    End Sub
End Class