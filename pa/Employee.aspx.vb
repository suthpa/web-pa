﻿Imports System.Data
Public Class Employee
    Inherits System.Web.UI.Page
    Dim ctlEmp As New EmployeeController
    Public dtC As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iPA")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("iPA")("ROLE_ADM") = False And Request.Cookies("iPA")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadEmployee()
        End If
    End Sub
    Private Sub LoadEmployee()
        dtC = ctlEmp.Employee_Get
    End Sub

End Class