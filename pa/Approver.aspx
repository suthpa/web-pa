﻿<%@ Page Title="Approver" MetaDescription="ข้อมูลผู้อนุมัติ" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Approver.aspx.vb" Inherits="SUTHPA.Approver" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user-female icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-warm-flame">
            </i>รายชื่อ Approver User
            <div class="btn-actions-pane-right">
                <a href="ApproverModify?m=u&s=cus" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
            </div>
        </div>
        <div class="card-body">
            <table id="tbdata" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="sorting_asc_disabled"></th>
                        <th>Username</th>
                        <th>ขื่อ-นามสกุล</th>
                        <th>เบอร์โทร</th>
                        <th>E-mail</th>
                        <th>สถานะ</th>
                    </tr>
                </thead>
                <tbody>
                    <% For Each row As DataRow In dtApv.Rows %>
                        <tr>
                            <td width="30"><a class="editcus" href="ApproverModify?m=u&s=apv&uid=<% =String.Concat(row("UserID")) %>" ><img src="images/icon-edit.png" /></a>
                            </td>
                            <td><%=String.Concat(row("Username")) %></td>
                            <td><%=String.Concat(row("Fname")) + " " + String.Concat(row("Lname")) %></td>
                            <td><%=String.Concat(row("Telephone")) %></td>
                            <td><%=String.Concat(row("Email")) %></td>
                            <td><%=IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %></td>
                        </tr>
                        <% Next %>
                </tbody>
            </table>
        </div>
        <div class="d-block text-right card-footer">
        </div>
    </div>
</section>
</asp:Content>