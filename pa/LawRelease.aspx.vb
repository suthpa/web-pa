﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawRelease
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iPA")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            Request.Cookies("iPA")("Legalimg1") = ""
            Request.Cookies("iPA")("picname1") = ""
            Request.Cookies("iPA")("Legalimg2") = ""
            Request.Cookies("iPA")("picname2") = ""
            Request.Cookies("iPA")("Legalimg3") = ""
            Request.Cookies("iPA")("picname3") = ""

            LoadAnalyst()

            LoadLegalData()

        End If


    End Sub

    Private Sub LoadAnalyst()
        dt = ctlU.Analyst_GetActive(Request.Cookies("iPA")("LoginCompanyUID"))
        'ddlUser.DataTextField = "Name"
        'ddlUser.DataValueField = "UserID"
        'ddlUser.DataBind()
        With grdUser
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Sub LoadLegalData()
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                LoadPracticeToGrid()
            End With
        Else
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        'Dim BDate As String = ddlDay.SelectedValue
        'BDate = ddlYear.SelectedValue & ddlMonth.SelectedValue & BDate
        'If txtGPAX.Text <> "" Then
        '    If Not IsNumeric(txtGPAX.Text) Then
        '        DisplayMessage(Me.Page, "GPAX ไม่ถูกต้อง")
        '        Exit Sub
        '    End If
        'End If

        'If ddlUser.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้เลือก User');", True)
        '    Exit Sub
        'End If

        'ctlL.Legal_Save(StrNull2Zero(hdLegalUID.Value), txtCode.Text, txtName.Text, StrNull2Zero(ddlType.SelectedValue), StrNull2Zero(ddlLawMaster.SelectedValue), StrNull2Zero(ddlOrganize.SelectedValue), StrNull2Zero(ddlArea.SelectedValue), StrNull2Zero(optPhase.SelectedValue), txtIssueDate.Text, txtStartDate.Text, txtRegisDate.Text, StrNull2Zero(ddlUser.SelectedValue), StrNull2Zero(txtYear.Text), StrNull2Zero(ddlBusinessType.SelectedValue), StrNull2Zero(ddlMinistry.SelectedValue), sKeyword, txtDescription.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iPA")("userid"))

        Dim LegalUID As Integer

        'If StrNull2Zero(hdLegalUID.Value) = 0 Then
        '    ctlM.RunningNumber_Update(CODE_LEGAL)
        '    LegalUID = ctlL.Legal_GetUID(txtCode.Text)
        'Else
        '    LegalUID = hdLegalUID.Value
        'End If

        'ctlL.LegalCategoryDetail_Delete(LegalUID)

        'For i = 0 To chkCategory.Items.Count - 1
        '    If chkCategory.Items(i).Selected Then
        '        ctlL.LegalCategoryDetail_Save(LegalUID, chkCategory.Items(i).Value, Request.Cookies("iPA")("userid"))
        '    End If
        'Next

        'ctlL.LegalKeyword_Delete(LegalUID)

        'For i = 0 To chkKeyword.Items.Count - 1
        '    If chkKeyword.Items(i).Selected Then
        '        ctlL.LegalKeyword_Save(LegalUID, chkKeyword.Items(i).Value, Request.Cookies("iPA")("userid"))
        '    End If
        'Next



        'ctlU.User_GenLogfile(Request.Cookies("iPA")("username"), ACTTYPE_UPD, "Legal", "บันทึก/แก้ไข Legal :{LegalUID=" & hdLegalUID.Value & "}{LegalCode=" & txtCode.Text & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub
    Private Sub LoadPracticeToGrid()
        dt = ctlL.Practice_Get(StrNull2Zero(hdLegalUID.Value))
        With grdPractice
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
    End Sub
End Class