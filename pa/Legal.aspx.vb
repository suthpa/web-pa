﻿Imports System.Data
Public Class Legal
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iPA")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadLegal()
        End If
    End Sub
    Private Sub LoadLegal()

        If Convert.ToBoolean(Request.Cookies("iPA")("ROLE_CUS")) = True Or Convert.ToBoolean(Request.Cookies("iPA")("ROLE_CAD")) = True Then
            ctlL.Legal_GetByCompany(Request.Cookies("iPA")("LoginCompanyUID"))
        Else
            If Request("m") = "apv" Then
                dtLegal = ctlL.Legal_GetByStatus(ACTION_WAIT)
            Else
                dtLegal = ctlL.Legal_Get()
            End If
        End If
    End Sub

End Class