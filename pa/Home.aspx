﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-light icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Dashboard
                                    <div class="page-title-subheading">ระบบประเมินผลการทำงานพนักงาน</div>
                                </div>
                            </div>
                        </div>
                    </div>

  <!-- Main content -->
  <section class="content">

      <h5>จำนวนพนักงานทั้งหมด</h5>
      <div class="row">
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <div class="chartjs-size-monitor">
                                                    <div class="chartjs-size-monitor-expand">
                                                        <div class=""></div></div>
                                                    <div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                                <h5 class="card-title">แยกตามสถานะ</h5> 
                                                <canvas id="canvas-ila-pie" style="display: block; width: 501px; height: 250px;" class="chartjs-render-monitor"></canvas>
                                            </div>
                                        </div>
                                      
                                    </div>

                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                                <h5 class="card-title">แยกตามประเภท</h5>
                                                <canvas id="canvas-ila-bar" class="chartjs-render-monitor" style="display: block; height: 210px; width: 420px;"></canvas>
                                            </div>
                                        </div>

                                       
                                    </div>
                                </div>

    <div class="row">
      <section class="col-lg-6 connectedSortable">

          <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <h5 class="card-title">Shortcut Menu</h5>
                                            <div class="grid-menu grid-menu-3col">
                                                <div class="no-gutters row">
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-license btn-icon-wrapper"> </i>จำนวนพนักงานทั้งหมด
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-magic-wand btn-icon-wrapper"> </i>ภาพรวมการ Actionพนักงาน
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-sad btn-icon-wrapper"> </i>พนักงานที่ไม่สอดคล้อง
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-smile btn-icon-wrapper"> </i>พนักงานที่สอดคล้อง
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-calendar-full btn-icon-wrapper"> </i>ปฏิทินรายการที่ต้องดำเนินการ
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                        <button class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-lighter btn-icon-wrapper"> </i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>
                                    </div>


        <div class="box-nobg no-border">
          <div class="box-body">
<!-- 
<div class="row">       
  
        <div class="col-md-2">
         <a class="icon-menu1" href="PersonModify?ActionType=bio&t=m">
             <div class="icon-imenu1"><div><img src="images/menu/law_book.png" alt="" width="64"></div>
              <div class="icon-menu1-name1">จำนวนพนักงาน<br /> ทั้งหมด</div>
          </div></a>

</div>       

          <div class="col-md-2">
            <a class="icon-menu1" href="PersonalHealth?ActionType=g&t=m"><div class="icon-imenu1"><div><img src="images/menu/calendar.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">ปฏิทินรายการที่<br />  ต้องดำเนินการ</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="Evaluated?ActionType=agrpt&pid=<% =Request.Cookies("iPA")("LoginPersonUID") %>">
                 <div class="icon-imenu1">
                <div><img src="images/menu/law.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">ภาพรวมการ Action<br />พนักงาน</div>
          </div></a></div>
       </div>
              
<div class="row">  
          <div class="col-md-2">
            <a class="icon-menu1" href="HRAResult?ActionType=hrarpt&pid=<% =Request.Cookies("iPA")("LoginPersonUID") %>"><div class="icon-imenu1"><div><img
                src="images/menu/document1.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">พนักงานที่<br />ไม่สอดคล้อง</div>
          </div></a></div>
 
          <div class="col-md-2">
            <a class="icon-menu1" href="ChangePassword.aspx?ActionType=chg">
                <div class="icon-imenu1"><div><img src="images/menu/document2.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">พนักงาน<br />ที่สอดคล้อง</div>
          </div></a></div>

 </div>
      -->        
          
  </div>  
      </div>   
      
      </section>
     
        <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar2"></div>
          </div>
        </div>


         

      </section>
    </div>
</section>

    
   
</asp:Content>