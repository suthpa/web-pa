﻿<%@ Page Title="Employee" MetaDescription="ข้อมูลพนักงาน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Employee.aspx.vb" Inherits="Employee" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-users icon-gradient bg-success">
            </i>ข้อมูลรายชื่อพนักงาน
            <div class="btn-actions-pane-right">
                <a href="CompanyModify?m=comp" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
            </div>
        </div>
        <div class="card-body">                  
              <table id="tbdata" class="table table-hover table-striped table-bordered dataTable dtr-inline">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>รหัสพนักงาน</th>
                  <th>ชื่อ-นามสกุล</th>
                  <th>ตำแหน่ง</th>
                  <th>แผนก</th>  
                    <th>ฝ่าย</th>
                     <th>สถานะ</th>
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtC.Rows %>
                <tr>
                 <td width="30px"><a  href="CompanyModify?cid=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                    </td>
                  <td><% =String.Concat(row("EmployeeID")) %></td>
                  <td><% =String.Concat(row("Name")) %>    </td>
                  <td><% =String.Concat(row("PositionName")) %></td>
                  <td><% =String.Concat(row("DepartmentName")) %></td>
                    <td class="text-center"><% =String.Concat(row("DivisionName")) %></td>
                    <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
  
    </section>
</asp:Content>
