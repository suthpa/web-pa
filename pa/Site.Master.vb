﻿Imports System.IO
Imports Newtonsoft.Json
Public Class Site
    Inherits System.Web.UI.MasterPage
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("iPA")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadUserDetail()
        End If
        LoadCalendarData()
    End Sub

    Private Sub LoadUserDetail()
        Dim ctlU As New EmployeeController
        dt = ctlU.Employee_GetByCode(Request.Cookies("iPA")("EmployeeID"))
        If dt.Rows.Count > 0 Then
            lblUserName1.Text = String.Concat(dt.Rows(0)("Title")) + String.Concat(dt.Rows(0)("Name"))
            lblUsername2.Text = String.Concat(dt.Rows(0)("Title")) + String.Concat(dt.Rows(0)("Name"))

            lblProfileDesc.Text = Request.Cookies("iPA")("PositionName")
            lblUserDesc.Text = Request.Cookies("iPA")("PositionName")

            If DBNull2Str(dt.Rows(0).Item("EmployeeID")) <> "" Then
                Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureUser & "/" & dt.Rows(0).Item("EmployeeID") & ".jpg"))

                If objfile.Exists Then
                    imgUser1.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("EmployeeID") & ".jpg"
                    imgUser2.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("EmployeeID") & ".jpg"
                Else
                    imgUser1.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                    imgUser2.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                End If

            End If

        End If
        dt = Nothing
    End Sub

    Dim ctlE As New MasterController
    Dim dtCalendar As New DataTable
    Public Shared json As String
    Private Sub LoadCalendarData()
        'If Request.Cookies("iPA")("ROLE_APP") = True Or Request.Cookies("iPA")("ROLE_ADM") = True Or Request.Cookies("iPA")("ROLE_SPA") = True Then
        '    dtCalendar = ctlE.Event_GetByAdmin(StrNull2Zero(Request.Cookies("iPA")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iPA")("LoginPersonUID")))
        'Else
        '    dtCalendar = ctlE.Event_GetByUser(StrNull2Zero(Request.Cookies("iPA")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iPA")("LoginPersonUID")))
        'End If

        dtCalendar = ctlE.Event_Get

        json = JsonConvert.SerializeObject(dtCalendar, Formatting.Indented)
    End Sub



End Class