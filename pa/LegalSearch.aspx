﻿<%@ Page Title="ค้นหากฎหมาย" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LegalSearch.aspx.vb" Inherits="LegalSearch" %>
<%@ Import Namespace="System.Data" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    

 <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-search icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Legal Search
                            <div class="page-title-subheading">ค้นหากฎหมาย </div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content">  
      <div class="row">  
   <div class="col-md-12 text-center">
             <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                               <% If Request("s") = "1"  %>
                                   <a  class="nav-link active"  href="LegalSearch.aspx?m=find&s=1"><span>รายการกฎหมายทั้งหมด</span></a>
                               <% Else %>
                                   <a  class="nav-link" href="LegalSearch.aspx?m=find&s=1"><span>รายการกฎหมายทั้งหมด</span></a>
                               <% End If %>
                            </li>
                            <li class="nav-item">
                               <% If Request("s") = "2"  %>
                                   <a  class="nav-link active"  href="LegalSearch.aspx?m=find&s=2"><span>ค้นหาตามกระทรวง</span></a>
                               <% Else %>
                                   <a  class="nav-link" href="LegalSearch.aspx?m=find&s=2"><span>ค้นหาตามกระทรวง</span></a>
                               <% End If %>                               
                            </li>
                            <li class="nav-item">
                               <% If Request("s") = "3"  %>
                                   <a  class="nav-link active"  href="LegalSearch.aspx?m=find&s=3"><span>ค้นหาตามปี</span></a>
                               <% Else %>
                                   <a  class="nav-link" href="LegalSearch.aspx?m=find&s=3"><span>ค้นหาตามปี</span></a>
                               <% End If %>
                            </li>
                
                  <li class="nav-item">
                               <% If Request("s") = "4"  %>
                                   <a  class="nav-link active"  href="LegalSearch.aspx?m=find&s=4"><span>ค้นหาตามประเภทธุรกิจ/อุตสาหกรรม</span></a>
                               <% Else %>
                                   <a  class="nav-link" href="LegalSearch.aspx?m=find&s=4"><span>ค้นหาตามประเภทธุรกิจ/อุตสาหกรรม</span></a>
                               <% End If %>
                            </li>
                   <li class="nav-item">
                               <% If Request("s") = "5"  %>
                                   <a  class="nav-link active"  href="LegalSearch.aspx?m=find&s=5"><span>ค้นหาตามคำสำคัญ</span></a>
                               <% Else %>
                                   <a  class="nav-link" href="LegalSearch.aspx?m=find&s=5"><span>ค้นหาตามคำสำคัญ</span></a>
                               <% End If %>                                 
                            </li>
              </ul> 
        </div>
      </div>
 

      <div class="row">  
           <div class="col-md-12">
            <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon pe-7s-search icon-gradient bg-success">
            </i>ค้นหา
            <div class="btn-actions-pane-right">               
            </div>
        </div>
        <div class="card-body">
             <div class="row">
                 <% If Request("s") = "2"  %>
                 <div class="col-md-4">
                      <div class="form-group">
                        <label>กระทรวง</label>
                        <asp:DropDownList ID="ddlMinistry" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>
                 <% End If %> 
                 <% If Request("s") = "3"Then  %>
                  <div class="col-md-2">
                     <div class="form-group">
                        <label>ปี</label>
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2"></asp:DropDownList>
                     </div>
                  </div>
                 <% End If %> 
                 <% If Request("s") = "4"  %>
                 <div class="col-md-4">
                      <div class="form-group">
                        <label>ประเภทธุรกิจ</label>
                        <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>
                 
                 <% End If %> 
                 <% If Request("s") = "5"  %>
                 <div class="col-md-12">
                      <div class="form-group">
                        <label>คำสำคัญ</label>
                        <asp:CheckBoxList ID="chkKeyword" runat="server" CssClass="check-button" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList>
                      </div>
                  </div>
                 <% End If %> 
                  <div class="col-md-4">
                      <div class="form-group">
                        <label>ระยะดำเนินการ</label>
                        <asp:DropDownList ID="ddlPhase" runat="server" CssClass="form-control select2">
                            <asp:ListItem Value="0">---ทั้งหมด---</asp:ListItem>
                            <asp:ListItem Value="1">ระยะเตรียมการ,ประกอบการ</asp:ListItem>
                                                <asp:ListItem Value="2">ระยะก่อสร้างโรงงาน/ดัดแปลง/รื้อถอน</asp:ListItem>
                                                <asp:ListItem Value="3">ระยะการทดลองเดินเครื่องจักร</asp:ListItem>
                                                <asp:ListItem Value="4">ระยะประกอบกิจการ</asp:ListItem>
                                                <asp:ListItem Value="5">ระยะยกเลิก/เปลี่ยนแปลงการประกอบการ</asp:ListItem>
                        </asp:DropDownList>
                      </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                         <br />
                        <asp:Button ID="cmdSearch" CssClass="btn btn-warning" runat="server" Text="ค้นหา" Width="120px" />
                         <% If Request("s") = "5"  %>
                            <asp:Button ID="cmdCancel" CssClass="btn btn-secondary" runat="server" Text="ยกเลิก" Width="120px" />        
                         <% End If %> 
                     </div>
                  </div>

            </div>
        </div>
       </div>
</div>
        </div>
  
     <div class="row">  
          <div class="col-md-12">

        <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-diamond icon-gradient bg-success">
            </i>กฎหมายที่เกี่ยวข้อง
            <div class="btn-actions-pane-right">
               
            </div>
        </div>
        <div class="card-body">  
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>                   
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center">ลำดับ</th>
                  <th class="text-center">ชื่อกฎหมาย</th>
                  <th class="text-center">ประเภท</th> 
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center">รายละเอียด</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>                  
                  <td  width="40" class="text-center"><% =String.Concat(row("RowNo")) %></td>
                  <td><% =String.Concat(row("LegalName")) %>    </td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                  <td class="text-center"> 
                      <a href="LegalModify?m=v&id=<% =String.Concat(row("UID")) %>" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i> ดูรายละเอียด</a></td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div> 
          </div>
 </div>
</div>
    </section>
</asp:Content>
