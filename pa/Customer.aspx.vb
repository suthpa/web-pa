﻿Imports System.Data
Public Class Customer
    Inherits System.Web.UI.Page
    Dim ctlEmp As New CustomerController
    Public dtCus As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        If Request.Cookies("iLaw")("ROLE_ADM") = False And Request.Cookies("iLaw")("ROLE_SPA") = False Then
            dtCus = ctlEmp.Customer_GetByCompanyUID(Request.Cookies("iLaw")("LoginCompanyUID"))
        Else
            dtCus = ctlEmp.Customer_GetAll
        End If
    End Sub
End Class