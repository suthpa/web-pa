﻿Public Class CustomerModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CustomerController
    Dim ctlA As New ApproverController
    Dim ctlC As New CompanyController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblAlert1.Visible = False
            lblAlert2.Visible = False

            LoadCompany()
            LoadApprover()

            If Not Request("uid") Is Nothing Then
                '    If Request("t") = "m" Then
                '        LoadCustomerData(Request.Cookies("iLaw")("LoginCustomerUID"))

                '    Else
                '        ' เป็นการเพิ่มใหม่ ตรวจสอบ limit 
                '        If Request.Cookies("iLaw")("ROLE_ADM") = True Or Request.Cookies("iLaw")("ROLE_SPA") = True Then

                '        Else
                '            If ctlC.Company_CheckOverMaxLimit(Request.Cookies("iLaw")("LoginCompanyUID")) = True Then
                '                lblAlert1.Visible = True
                '                lblAlert2.Visible = True
                '                cmdSave.Visible = False
                '            Else
                '                cmdSave.Visible = True
                '                lblAlert1.Visible = False
                '                lblAlert2.Visible = False
                '            End If


                '        End If

                '    End If
                'Else
                '    ''LoadCustomerData(Request.Cookies("iLaw")("LoginCustomerUID")) 'Request.Cookies("iLaw")("LoginCustomerUID")
                LoadCustomerData(Request("uid"))
            End If


            If Request.Cookies("iLaw")("ROLE_ADM") = True Then
                ddlCompany.Enabled = True
                cmdDel.Visible = True
            Else
                ddlCompany.Enabled = False
                cmdDel.Visible = False
            End If

        End If
    End Sub
    Private Sub LoadCompany()

        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("iLaw")("LoginCompanyUID")

    End Sub
    Private Sub LoadApprover()
        ddlApprover.DataSource = ctlA.Approver_GetActive()
        ddlApprover.DataTextField = "Name"
        ddlApprover.DataValueField = "UserID"
        ddlApprover.DataBind()
    End Sub

    Private Sub LoadCustomerData(CustomerUID As Integer)
        dt = ctlE.Customer_GetByCustomerID(CustomerUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdCustomerUID.Value = String.Concat(.Item("CustomerUID"))
                txtFirstName.Text = String.Concat(.Item("FName"))
                txtLastName.Text = String.Concat(.Item("Lname"))
                txtTel.Text = String.Concat(.Item("Telephone"))
                txtEmail.Text = String.Concat(.Item("Email"))
                ddlCompany.SelectedValue = String.Concat(.Item("CompanyUID"))

                txtUsername.Text = String.Concat(.Item("Username"))
                txtPassword.Text = enc.DecryptString( String.Concat(.Item("Passwords")),True)

                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                LoadApproverToGrid()
            End With

        Else
        End If
    End Sub

    Dim ctlU As New EmployeeController

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If ddlCompany.SelectedIndex = -1 Or ddlCompany.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกหน่วยงานก่อน');", True)
            Exit Sub
        Else
            If StrNull2Zero(hdCustomerUID.Value) = 0 Then
                If ctlC.Company_CheckOverMaxLimit(StrNull2Zero(ddlCompany.SelectedValue)) = True Then
                    ' เกิน limit ที่ซื้อไว้
                    lblAlert1.Visible = True
                    lblAlert2.Visible = True
                    cmdSave.Visible = False
                    cmdDel.Visible = False
                    Exit Sub
                Else
                    cmdSave.Visible = True
                    cmdDel.Visible = True
                    lblAlert1.Visible = False
                    lblAlert2.Visible = False
                End If
            End If
        End If

        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกชื่อ');", True)
            Exit Sub
        End If
        If txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกนามสกุล');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก E-mail');", True)
            Exit Sub
        End If

        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก Username');", True)
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Password');", True)
            Exit Sub
        End If

        ctlE.Customer_Save(StrNull2Zero(hdCustomerUID.Value), txtUsername.Text, enc.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, StrNull2Zero(ddlCompany.SelectedValue), txtTel.Text, txtEmail.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iLaw")("userid"))


        '--------------Update Approver-----------------
        Dim iCustomerUID As Integer

        If StrNull2Zero(hdCustomerUID.Value) = 0 Then
            If txtUsername.Text <> "" Then
                iCustomerUID = ctlE.Customer_GetCustomerUID(txtUsername.Text, txtFirstName.Text.Trim())
                ctlA.ApproverCustomer_UpdateCustomerUID(txtUsername.Text, iCustomerUID)
            End If
        End If
        '-----------------End Add -----------

        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข Customer User :{uid=" & hdCustomerUID.Value & "}{username=" & txtUsername.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("ResultPage.aspx?p=cus")

    End Sub

    Protected Sub cmdAddApprover_Click(sender As Object, e As EventArgs) Handles cmdAddApprover.Click
        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากำหนด Username ก่อน');", True)
            Exit Sub
        End If
        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลผู้ใช้ให้ครบถ้วนก่อน');", True)
            Exit Sub
        End If
        '--------------Add Approver-----------------
        Dim iCustomerUID As Integer = StrNull2Zero(hdCustomerUID.Value)
        ctlA.ApproverCustomer_Save(ddlApprover.SelectedValue, iCustomerUID, txtUsername.Text, Request.Cookies("iLaw")("UserID"))
        '-----------------End Add -----------
        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Approver", "บันทึก/แก้ไข Approver Asignment :{uid=" & hdCustomerUID.Value & "}{username=" & txtUsername.Text & "}{approver=" & ddlApprover.SelectedValue & "}", "")

        LoadApproverToGrid()
    End Sub

    Private Sub LoadApproverToGrid()
        Dim dtA As New DataTable
        dtA = ctlA.Approver_GetByCustomerUID(StrNull2Zero(hdCustomerUID.Value))
        If dtA.Rows.Count > 0 Then
            With grdApprover
                .Visible = True
                .DataSource = dtA
                .DataBind()
            End With
        Else
            grdApprover.DataSource = Nothing
            grdApprover.Visible = False
        End If
        dtA = Nothing

    End Sub
    Protected Sub grdApprover_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdApprover.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    If ctlA.ApproverCustomer_Delete(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), "DEL", "Approver", "ลบ Approver Customer :" & e.CommandArgument, "")
                        LoadApproverToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub
End Class