﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawRelease.aspx.vb"
    Inherits="LawRelease" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Legal Release
                            <div class="page-title-subheading">Release พนักงานให้ User </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">  
                        
                         <div class="app-page-header">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">                      
                        <div><asp:label ID="lblLegalName" runat="server"></asp:label><asp:HiddenField ID="hdLegalUID" runat="server" />
                            <div class="page-title-subheading">รหัสพนักงาน <asp:label ID="lblLegalCode" runat="server"></asp:label></div>
                        </div>
                    </div>
                </div>
            </div>

  <div class="box box-success collapsed-box">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>
              <h3 class="box-title">เลือก User</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button> 
              </div>                                 
            </div>
            <div class="box-body"> 
                          <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label></label>
                                   <asp:GridView ID="grdUser"  runat="server" AutoGenerateColumns="False" Width="100%"  CssClass="table table-hover scrollbar-container" Height="50px" BorderStyle="None" GridLines="None">            
            <columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelectU" runat="server" />
                </ItemTemplate>
              <itemstyle Width="30px" />          
            </asp:TemplateField>
            <asp:BoundField HeaderText="ชื่อ" DataField="Name">
             </asp:BoundField>
                <asp:BoundField DataField="UserID" HeaderText="แผนก">             
                </asp:BoundField>
            </columns>              
          </asp:GridView>

   
                                             <!--    cssclass="multiselect-dropdown form-control" data-select2-id="1" tabindex="-1" aria-hidden="true"  -->
                                        </div>
                                    </div> 
                                </div>                                                            

                            </div>
                           
                        </div>
                    </section>
                </div>
                <div class="row">
                    <section class="col-lg-12 connectedSortable">
                        <div class="main-card mb-3 card">
                            <div class="card-header">ข้อสาระสำคัญที่ต้องปฏิบัติ</div>
                            <div class="card-body">
                                <div class="row">
                                   <asp:GridView ID="grdPractice" 
                             runat="server" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover" BorderStyle="None" GridLines="None">
            <RowStyle HorizontalAlign="Center" />
            <columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:BoundField HeaderText="ลำดับ" DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" /></asp:BoundField>
                <asp:BoundField DataField="Code" HeaderText="รหัส">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
<asp:BoundField DataField="Descriptions" HeaderText="ข้อสาระสำคัญ">
                <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
                <asp:BoundField DataField="RecurrenceText" HeaderText="ความถี่" />
            </columns>
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <headerstyle CssClass="th"                  VerticalAlign="Middle" />          
          </asp:GridView>



                                </div>

                            </div>
                            <div class="box-footer clearfix">

                            </div>
                        </div>
                        
                    </section>
                </div>

                <div class="row justify-content-center">               
                    <div class="col-md-12 text-center">
                        <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />             

                    </div>
                </div>

            </section>
        </asp:Content>