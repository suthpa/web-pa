﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawEssence.aspx.vb"
    Inherits="iLA.LawEssence" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">     
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>สาระสำคัญที่ต้องปฏิบัติ
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        <div class="main-card mb-3 card">
                            <div class="card-header">ทะเบียนกฎหมาย<asp:HiddenField ID="hdLegalUID" runat="server" />
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>รหัส</label>
                                            <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center" placeholder="Code"></asp:TextBox>
                                        </div>
                                    </div>                               
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>ชื่อ</label>
                                            <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="Legal name"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>กฎหมายแม่บท</label>
                                            <asp:DropDownList ID="ddlLawMaster" runat="server"  cssclass="form-control select2" Width="100%">
                                            </asp:DropDownList>
                                        </div>

                                    </div>        
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ประเภทกฎหมาย</label>
                                            <asp:DropDownList ID="ddlType" runat="server"  cssclass="form-control select2"> 
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                        </div>  

                            </div>
                            <div class="d-block text-right card-footer">

                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <section class="col-lg-12 connectedSortable">
                        <div class="main-card mb-3 card">
                            <div class="card-header">สาระสำคัญที่ต้องปฏิบัติ</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>สิ่งที่กฎหมายให้ปฏิบัติ</label>
                                            <div class="input-group">                                             
                                                <asp:TextBox ID="txtIssueDate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>   
                                </div> 
                                
                                           <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Implementation phase</label>
                                            <asp:RadioButtonList ID="optPhase" runat="server"
                                                RepeatDirection="Vertical">
                                                <asp:ListItem Value="1">ระยะเตรียมการ,ประกอบการ</asp:ListItem>
                                                <asp:ListItem Value="2">ระยะก่อสร้างโรงงาน/ดัดแปลง/รื้อถอน</asp:ListItem>
                                                <asp:ListItem Value="3">ระยะการทดลองเดินเครื่องจักร</asp:ListItem>
                                                <asp:ListItem Value="4">ระยะประกอบกิจการ</asp:ListItem>
                                                <asp:ListItem Value="5">ระยะยกเลิก/เปลี่ยนแปลงการประกอบการ
                                                </asp:ListItem>
                                            </asp:RadioButtonList>

                                        </div>

                                    </div>
                                </div>
                             <div class="row">   
                    <div class="col-md-12">
                        <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" /> 
                    </div>
                </div>
                                 <div class="row">   
                    <div class="col-md-12"> 
                           <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="20" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="ID" DataField="DepartmentUID">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
<asp:BoundField DataField="DepartmentCode" HeaderText="Code">
                <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
            <asp:BoundField DataField="DepartmentName" HeaderText="Name">
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
            
            <asp:TemplateField HeaderText="Active">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# iLA.ConvertStatusFlag2CHK(DataBinder.Eval(Container.DataItem, "StatusFlag")) %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DepartmentUID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DepartmentUID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True"                  VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>


                    </div>
                </div>
                                
                            </div>
                            <div class="box-footer clearfix">

                            </div>
                        </div>

                    </section>
                </div>

              
            </section>
        </asp:Content>