﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LegalModify.aspx.vb"
    Inherits="LegalModify" %>

    <%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
        namespace="DevExpress.Web" tagprefix="dx" %>

        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Legal Management
                            <div class="page-title-subheading">จัดการรายละเอียดกฎหมาย </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        <div class="main-card mb-3 card">
                            <div class="card-header">ทะเบียนกฎหมาย<asp:HiddenField ID="hdLegalUID" runat="server" />
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>รหัส</label>
                                            <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center" placeholder="Code"></asp:TextBox>
                                        </div>
                                    </div>                               
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>ชื่อ</label>
                                            <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="Legal name"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>กฎหมายแม่บท</label>
                                            <asp:DropDownList ID="ddlLawMaster" runat="server"  cssclass="form-control select2" Width="100%">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>หน่วยงานที่บังคับใช้กฎหมาย</label>
                                            <asp:DropDownList ID="ddlOrganize" runat="server"
                                                cssclass="form-control select2" Width="100%">
                                            </asp:DropDownList>                                            
                                        </div>

                                    </div>
                               
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ประเภทกฎหมาย</label>
                                            <asp:DropDownList ID="ddlType" runat="server"  cssclass="form-control select2">
                                                <asp:ListItem Value="1">Safety</asp:ListItem>
                                                <asp:ListItem Value="2">Environment</asp:ListItem>
                                                <asp:ListItem Value="3">HR</asp:ListItem>
                                                <asp:ListItem Value="4">BOI</asp:ListItem>
                                                <asp:ListItem Value="5">Finance</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>พื้นที่บังคับใช้</label>
                                            <asp:DropDownList ID="ddlArea" runat="server"
                                                cssclass="form-control select2">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                        </div>
                                       <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mailbox-messages">
                                            <label>ขอบเขตการบังคับใช้</label>
                                            <asp:CheckBoxList ID="chkCategory" runat="server" CssClass="mailbox-messages" RepeatColumns="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    </div>
                                  <div class="row">
                                        <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <asp:TextBox ID="txtYear" runat="server" cssclass="form-control text-center" placeholder=""></asp:TextBox>
                                        </div>
                                    </div>
                                       <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ประเภทกิจการ</label>
                                            <asp:DropDownList ID="ddlBusinessType" runat="server" cssclass="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ประเภทโรงงาน</label>
                                            <asp:DropDownList ID="ddlBusinessGoup" runat="server" CssClass="form-control select2">
                                                 <asp:ListItem Value="1">โรงงานจำพวก 1</asp:ListItem>
                                                <asp:ListItem Value="2">โรงงานจำพวก 2</asp:ListItem>
                                                <asp:ListItem Value="3">โรงงานจำพวก 3</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label>หน่วยงาน</label>
                                            <asp:DropDownList ID="ddlMinistry" runat="server" cssclass="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                                   <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>สรุปเนื้อหา</label>
                                            <asp:TextBox ID="txtDescription" runat="server" cssclass="form-control"
                                                placeholder="Legal description" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                             
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mailbox-messages">
                                            <label>Keyword</label>
                                            <asp:CheckBoxList ID="chkKeyword" runat="server" CssClass="mailbox-messages" RepeatColumns="5">
                                            </asp:CheckBoxList>

                                        </div>

                                    </div>

                                    </div>

                                           <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Implementation phase</label>
                                            <asp:RadioButtonList ID="optPhase" runat="server"
                                                RepeatDirection="Vertical">
                                                <asp:ListItem Value="1">ระยะเตรียมการ,ประกอบการ</asp:ListItem>
                                                <asp:ListItem Value="2">ระยะก่อสร้างโรงงาน/ดัดแปลง/รื้อถอน</asp:ListItem>
                                                <asp:ListItem Value="3">ระยะการทดลองเดินเครื่องจักร</asp:ListItem>
                                                <asp:ListItem Value="4">ระยะประกอบกิจการ</asp:ListItem>
                                                <asp:ListItem Value="5">ระยะยกเลิก/เปลี่ยนแปลงการประกอบการ
                                                </asp:ListItem>
                                            </asp:RadioButtonList>

                                        </div>

                                    </div>
                                </div>


                            </div>
                            <div class="d-block text-right card-footer">

                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
                    <section class="col-lg-12 connectedSortable">
                        <div class="main-card mb-3 card">
                            <div class="card-header">Description</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่กฎหมายออก</label>
                                            <div class="input-group">                                             
                                                <asp:TextBox ID="txtIssueDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่มีผลบังคับ</label>
                                            <div class="input-group">                                               
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                        <span class="input-group-text"> <i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่ลงทะเบียน</label>
                                            <div class="input-group">                                               
                                                <asp:TextBox ID="txtRegisDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"> <i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ผู้วิเคราะห์กฎหมาย</label>
                                            <asp:DropDownList ID="ddlUser" runat="server"
                                                cssclass="form-control select2" Width="100%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>

                                    </div>



                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ไฟล์เอกสาร Original Law(PDF,word)</label>
                                            <asp:FileUpload ID="FileUpload1" runat="server" Width="80%" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label></label>
                                            <asp:HyperLink ID="hlnkDoc" runat="server" Target="_blank">[hlnkDoc]
                                            </asp:HyperLink>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">

                            </div>
                        </div>

                        <asp:Panel ID="pnPractice" runat="server">
                               <div class="main-card mb-3 card">
                            <div class="card-header">สาระสำคัญที่ต้องปฏิบัติ
                                   </div>
                            <div class="card-body">
                                  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                               <ContentTemplate>
                                <div class="row">
                              <div class="col-md-2">
                                        <div class="form-group">
                                            <label>รหัส</label>
                                            <div class="input-group">                                         
                                                <asp:TextBox ID="txtPracticeCode" runat="server" CssClass="form-control text-center"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>ความถี่</label>
                                            <div class="input-group">                                         
                                                 <asp:RadioButtonList ID="optRecurence" runat="server"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="O">Onetime action</asp:ListItem>
                                                <asp:ListItem Value="W">Weekly</asp:ListItem>
                                                <asp:ListItem Value="M">Monthly</asp:ListItem>
                                                <asp:ListItem Value="Y">Yearly</asp:ListItem>
                                                <asp:ListItem Value="2Y">Every two years</asp:ListItem>
                                            </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Duedate</label>
                                            <div class="input-group">                                             
                                                 <asp:TextBox ID="txtDuedate" runat="server" CssClass="form-control text-center"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                        <span class="input-group-text"> <i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>สิ่งที่กฎหมายให้ปฏิบัติ</label>
                                            <div class="input-group">                                             <asp:HiddenField ID="hdPracticeUID" runat="server" />
                                                <asp:TextBox ID="txtPracticeDescription" runat="server" CssClass="form-control" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                           </ContentTemplate>
                               <Triggers>
                                   <asp:AsyncPostBackTrigger ControlID="cmdAddPractice" EventName="Click" />
                                   <asp:AsyncPostBackTrigger ControlID="grdPractice" EventName="RowCommand" />
                               </Triggers>
                           </asp:UpdatePanel>
                                   <div class="row">   
                    <div class="col-md-12 text-center">
                        <asp:Button ID="cmdAddPractice" CssClass="btn btn-success" runat="server" Text="บันทึกข้อสาระสำคัญ" /> 
                    </div>
                </div>
                                 <div class="row">   
                    <div class="col-md-12"> 
                           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                               <ContentTemplate>
                                   
                           <asp:GridView ID="grdPractice" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="ลำดับ" DataField="nRow">
                <HeaderStyle HorizontalAlign="Center" />
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" /></asp:BoundField>
                <asp:BoundField DataField="Code" HeaderText="รหัส">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
<asp:BoundField DataField="Descriptions" HeaderText="ข้อสาระสำคัญ">
                <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
                <asp:BoundField DataField="RecurrenceText" HeaderText="ความถี่">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
             ImageUrl="images/delete.png" 
             CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True"                  VerticalAlign="Middle" HorizontalAlign="Center" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>

                               </ContentTemplate>
                               <Triggers>
                                   <asp:AsyncPostBackTrigger ControlID="cmdAddPractice" EventName="Click" />
                                   <asp:AsyncPostBackTrigger ControlID="grdPractice" EventName="RowCommand" />
                               </Triggers>
                           </asp:UpdatePanel>

                    </div>
                </div>      
                   </div>                        
                        </div>
                        </asp:Panel>

                    </section>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <asp:CheckBox ID="chkStatus" runat="server" Text="ใช้งาน" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
                         <asp:Button ID="cmdApprove" CssClass="btn btn-success" runat="server" Text="Approve" Width="120px" />
                        <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="Delete"
                            Width="120px" />

                    </div>
                </div>

            </section>
        </asp:Content>