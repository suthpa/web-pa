﻿<%@ Page Title="User Account" MetaDescription="จัดการข้อมูลผู้ใช้งาน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserModify.aspx.vb" Inherits="UserModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">  

<div class="row">    
<section class="col-lg-7 connectedSortable">
  <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-primary">
            </i>ข้อมูลทั่วไป
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">

      <div class="row">
            <div class="col-md-2">
          <div class="form-group">
            <label>User ID.</label>
              <asp:Label ID="lblUserID" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>
        </div>        
          <div class="col-md-5">
          <div class="form-group">
            <label>ชื่อ</label>
            <asp:TextBox ID="txtFirstName" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>นามสกุล</label>
            <asp:TextBox ID="txtLastName" runat="server" cssclass="form-control" placeholder="นามสกุล"></asp:TextBox>          
          </div>
        </div> 
      </div>
      <div class="row">          
           <div class="col-md-6">
          <div class="form-group">
            <label>เบอร์โทร</label>
            <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>E-mail</label>
            <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
          </div>
        </div>
      </div>             
         <div class="row">          
         <div class="col-md-12">
          <div class="form-group">
            <label>บริษัท</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>
        </div>
      </div> 
             <div class="row">          
         <div class="col-md-12">
          <div class="form-group">
            <label>หน่วยงาน</label>
            <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>
        </div>
      </div> 
  <div class="row"> 
             <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" Checked="True" />
          </div>
        </div>
   </div>
      </div>
    </div>   

</section>
    <section class="col-lg-5 connectedSortable"> 

  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-screen icon-gradient bg-primary">
            </i>รหัสผู้ใช้งาน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Username</label>
            <asp:TextBox ID="txtUsername" runat="server" cssclass="form-control text-center" placeholder="ชื่อ Login"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Password</label>
            <asp:TextBox ID="txtPassword" runat="server" cssclass="form-control text-center" placeholder="รหัสผ่าน"></asp:TextBox>
          </div>
      </div>
      </div>
</div>        
          </div> 
         <div class="main-card mb-3 card">
   <div class="card-header"><i class="header-icon lnr-lock icon-gradient bg-primary">
            </i>สิทธิ์การใช้งาน
            <div class="btn-actions-pane-right"> 
            </div>
        </div>
        <div class="card-body">
        <asp:CheckBoxList ID="chkGroup" runat="server">
        </asp:CheckBoxList> 
          </div>
    </div>
</section>
</div>  

  <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
              <asp:Button ID="cmdClear" CssClass="btn btn-secondary" runat="server" Text="Clear" Width="120px" />
            <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
        </div>
      </div>

        </section>
</asp:Content>
