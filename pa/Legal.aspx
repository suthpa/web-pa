﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Legal.aspx.vb" Inherits="Legal" %>
<%@ Import Namespace="System.Data" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    

    <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>ทะเบียนพนักงาน
                            <div class="page-title-subheading">รายการทะเบียนพนักงาน </div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-license icon-gradient bg-success">
            </i>Legal List
            <div class="btn-actions-pane-right">
                <% If Convert.ToBoolean(Request.Cookies("iPA")("ROLE_ADM")) = True Then%>  
                <a href="LegalModify?m=new"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> ลงทะเบียนพนักงานใหม่</a>    
                <% End If %>
            </div>
        </div>     
              <div class="card-body">   
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th class="text-center">Code</th>
                  <th class="text-center">ชื่อพนักงาน</th>
                  <th class="text-center">ประเภท</th>
                  <th class="text-center">สถานะ</th>
                  <th class="text-center">ใช้งาน</th>
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center"></th>                                
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>               

                  <td class="text-center"><% =String.Concat(row("Code")) %></td>
                  <td><a  href="LegalDetail?m=v&id=<% =String.Concat(row("UID")) %>" ><% =String.Concat(row("LegalName")) %> </a>  </td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                  <td class="text-center"><% =String.Concat(row("ActionStatusName")) %></td>
                  <td class="text-center"><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>    
                    <td class="text-center"> 
                      <a href="LegalDetail?m=v&id=<% =String.Concat(row("UID")) %>" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i> ดูรายละเอียด</a>
                        <a href="LegalModify?lid=<% =String.Concat(row("UID")) %>" class="btn btn-primary" ><i class="fa fa-edit" aria-hidden="true"></i> แก้ไข</a>
                        <a  href="LawRelease?lid=<% =String.Concat(row("UID")) %>" class="btn btn-warning"><i class="fa fa-plus-circle" aria-hidden="true"></i> Release</a>
                    </td>

                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
 
  
    </section>
</asp:Content>
