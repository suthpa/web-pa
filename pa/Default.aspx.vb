﻿
Public Class PageDefault
    Inherits Page
    Dim dt As New DataTable
    Dim acc As New EmployeeController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Request("logout") = "y" Then
            Session.Contents.RemoveAll()
            Session.Abandon()
            Session.RemoveAll()
            Response.Cookies.Clear()

            Dim delCookie1 As HttpCookie
            delCookie1 = New HttpCookie("iPA")
            delCookie1.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(delCookie1)
        Else
            'RenewSessionFromCookie()
        End If

        If Not IsPostBack Then
            If Request("logout") = "y" Then
                Session.Abandon()
                Request.Cookies("iPA")("userid") = Nothing
            End If
            txtUsername.Focus()
        End If

        Session.Timeout = 360
        'Response.Redirect("Home")

    End Sub

    Private Sub CheckUser()

        dt = acc.Employee_Login(GlobalVariables.BYear, txtUsername.Text, txtPassword.Text)

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            '--------------------------------------------------------------------------------------------
            Dim row As DataRow = dt.Rows(0)

            Dim iLawCookie As HttpCookie = New HttpCookie("iPA")
            iLawCookie("UserID") = dt.Rows(0).Item("UID")
            iLawCookie("Username") = String.Concat(dt.Rows(0).Item("EmployeeID")).ToString()
            iLawCookie("LoginUser") = String.Concat(dt.Rows(0).Item("EmployeeID")).ToString()
            iLawCookie("EmployeeID") = String.Concat(dt.Rows(0).Item("EmployeeID")).ToString()
            'iLawCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            'iLawCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            iLawCookie("NameOfUser") = row("Title").ToString() & " " + row("Name").ToString()

            iLawCookie("AssessorLevelUID") = dt.Rows(0).Item("EmployeeLevelID")
            iLawCookie("EmployeeLevelName") = dt.Rows(0).Item("EmployeeLevelName")

            iLawCookie("ROLE_ADM") = False
            iLawCookie("ROLE_HR") = False
            iLawCookie("ROLE_DIR") = False
            iLawCookie("ROLE_SPA") = False

            iLawCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(iLawCookie)


            'If Request.Cookies("iPA")("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    Request.Cookies("iPA")("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(Request.Cookies("iPA")("ProfileID"))
            'End If

            'Dim ctlCfg As New SystemConfigController()
            'Request.Cookies("iPA")("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'Request.Cookies("iPA")("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            'genLastLog()
            'acc.User_GenLogfile(txtUsername.Text, ACTTYPE_LOG, "Users", "", "")



            GlobalVariables.username = row("EmployeeID").ToString()
            GlobalVariables.usercode = row("UID").ToString()
            GlobalVariables.LoginUser = row("EmployeeID").ToString()
            GlobalVariables.UserEmployeeName = row("Title").ToString() & " " + row("Name").ToString()
            GlobalVariables.AssessorLevelUID = Convert.ToInt32(row("EmployeeLevelID"))
            GlobalVariables.UserEmployeeLevelName = row("EmployeeLevelName").ToString()
            GlobalVariables.IsHR = False
            GlobalVariables.IsDirector = False
            GlobalVariables.IsSuperAdmin = False

            If row("StatusFlag").ToString() <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username นี้ระงับการใช้งาน กรุณาติดต่อแผนกสารสนเทศ (IT) โทร.7620');", True)
            End If

            If row("isHR").ToString() = "Y" Then
                iLawCookie("ROLE_HR") = True
                Request.Cookies("iPA")("ROLE_HR") = True
            End If

            If row("isDirector").ToString() = "Y" Then
                GlobalVariables.IsDirector = True
                iLawCookie("ROLE_DIR") = True
                Request.Cookies("iPA")("ROLE_DIR") = True
            End If

            If row("isSuperAdmin").ToString() = "Y" Then
                GlobalVariables.IsSuperAdmin = True
                iLawCookie("ROLE_SPA") = True
                Request.Cookies("iPA")("ROLE_SPA") = True
            End If

            If row("isAdmin").ToString() = "Y" Then
                GlobalVariables.IsAdmin = True
                iLawCookie("ROLE_ADM") = True
                Request.Cookies("iPA")("ROLE_ADM") = True
            Else
                GlobalVariables.IsAdmin = False
                iLawCookie("ROLE_ADM") = False
                Request.Cookies("iPA")("ROLE_ADM") = False
            End If

            If row("isReporter").ToString() = "Y" Then
                GlobalVariables.IsReporter = True
            Else
                GlobalVariables.IsReporter = False
            End If


            'Select Case Request.Cookies("iPA")("UserProfileID")
            '    Case 1 'Customer
            '        Response.Redirect("Default.aspx")
            '    Case 2, 3 'Teacher
            '        Response.Redirect("Default.aspx")
            '    Case 4 'Admin
            Response.Redirect("Home")
            'End Select

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        'txtUsername.Value = "admin"
        'txtPassword.Value = "112233"
        'Request.Cookies("iPA")("userid") = 1
        'Request.Cookies("iPA")("ROLE_SPA") = True
        'Response.Redirect("Home")
        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        'acc.User_LastLog_Update(txtUsername.Text)
    End Sub

End Class